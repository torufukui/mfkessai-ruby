# Mfkessai Ruby Library
mfkessaiのapiを使用するための便利なアクセスをRubyで提供します

こちらのgemは非公式なライブラリです

### 対応API

- [customer](#customer)
- [destination](#destination)
- [examination](#examination)
- [ping](#ping)
- [transaction](#transaction)

[詳しいAPIを参照する](https://mfkessai.co.jp/api_doc/endpoint.html)

## Installation

```
$ gem install mfkessai-ruby
```

## Usage

ライブラリは、利用可能なアカウントの秘密鍵と本番環境かテスト環境かのURLの設定をする必要があります。

Mfkessai.api_key, Mfkessai.api_urlをその値に設定します。

```
require "mfkessai"
Mfkessai.api_url = "https://sandbox"
Mfkessai.api_key = "mfkessai_test_..."

# list customers
Mfkessai::Customer.list

# retrieve single customer
Mfkessai::Customer.retrieve("customer_test_123")
```

## API一覧

## customer

- Mfkessai::Customer.list

```
require "mfkessai"
Mfkessai.api_url = "https://sandbox"
Mfkessai.api_key = "mfkessai_test_..."

# 顧客リストを取得
Mfkessai::Customer.list

# ページ番号、1ページあたりの件数を指定する
Mfkessai::Customer.list(page: 3, per_page: 30)
```

- Mfkessai::Customer.retrieve

```
require "mfkessai"
Mfkessai.api_url = "https://sandbox"
Mfkessai.api_key = "mfkessai_test_..."

# 顧客IDを指定して取得
Mfkessai::Customer.retrieve("customer_test_123")
```

## destination

- Mfkessai::Destination.create

```
require "mfkessai"
Mfkessai.api_url = "https://sandbox"
Mfkessai.api_key = "mfkessai_test_..."

# 請求先を登録
params = {
  "address1": "世田谷区1-2-3",
  "address2": "ビル3F",
  "cc_emails": [
    "another.test1@example.jp",
    "another.test2@example.jp"
  ],
  "customer": {
    "office_name": "テスト商事株式会社",
    "user_defined_id": "customer123456"
  },
  "customer_id": "ABCD-EFGH",
  "department": "経理部",
  "email": "test@example.jp",
  "name": "請求先氏名",
  "prefecture": "東京都",
  "tel": "03-1234-5678",
  "title": "",
  "zip_code": "111-1111"
}
Mfkessai::Destination.create(request_body: params)
```

## examination
- Mfkessai::Examination.list

```
require "mfkessai"
Mfkessai.api_url = "https://sandbox"
Mfkessai.api_key = "mfkessai_test_..."

# 審査リストを取得
Mfkessai::Examination.list

# ページ番号、1ページあたりの件数を指定する
Mfkessai::Examination.list(page: 3, per_page: 30)
```

- Mfkessai::Examination.create

```
require "mfkessai"
Mfkessai.api_url = "https://sandbox"
Mfkessai.api_key = "mfkessai_test_..."

# 審査のための取引情報を登録
params = {
  "amount": 2160,
  "destination_id": "XXXX-YYYY",
  "due_date": "2018-05-31T00:00:00+09:00",
  "examination_details": [
    {
      "amount": 2000,
      "description": "商品名A",
      "quantity": 2,
      "unit_price": 1000
    },
    {
      "amount": 160,
      "description": "消費税",
      "quantity": 1,
      "unit_price": 160
    }
  ]
}

Mfkessai::Examination.create(request_body: params)
```

- Mfkessai::Examination.retrieve

```
require "mfkessai"
Mfkessai.api_url = "https://sandbox"
Mfkessai.api_key = "mfkessai_test_..."

# IDを指定して審査情報を取得
Mfkessai::Examination.retrieve('destination_test_123')
```

- Mfkessai::Examination.create_transaction

```
require "mfkessai"
Mfkessai.api_url = "https://sandbox"
Mfkessai.api_key = "mfkessai_test_..."

# 審査時に登録した情報をもとに取引を登録

params = {
  "date": "2018-04-16T00:00:00+09:00",
  "email_flag": true,
  "issue_date": "2018-05-16T00:00:00+09:00",
  "posting_flag": false,
  "user_defined_id": "transaction00000001"
}

Mfkessai::Examination.create_transaction(id: 'test_123', request_body: params)
```

## ping

- Mfkessai::Ping.send

```
require "mfkessai"
Mfkessai.api_url = "https://sandbox"
Mfkessai.api_key = "mfkessai_test_..."

# pingを飛ばす
Mfkessai::Ping.send
```

## transaction

- Mfkessai::Transaction.list

```
require "mfkessai"
Mfkessai.api_url = "https://sandbox"
Mfkessai.api_key = "mfkessai_test_..."

# 取引リストを取得

Mfkessai::Transaction.list

# ページ番号、1ページあたりの件数を指定する
Mfkessai::Transaction.list(page: 3, per_page: 30)
```

- Mfkessai::Transaction.create

```
require "mfkessai"
Mfkessai.api_url = "https://sandbox"
Mfkessai.api_key = "mfkessai_test_..."

# 取引を登録
params = {
  "amount": 2160,
  "date": "2018-04-16T00:00:00+09:00",
  "destination": {
    "address1": "千代田区1-2-3",
    "address2": "サンプルビル3F",
    "cc_emails": [
      "another.test1@example.jp",
      "another.test2@example.jp"
    ],
    "customer": {
      "office_name": "サンプル商事株式会社",
      "user_defined_id": "customer123456"
    },
    "customer_id": "ABCD-EFGH",
    "department": "経理部",
    "email": "kesai.tanto@example.jp",
    "name": "請求先氏名",
    "prefecture": "東京都",
    "tel": "03-1234-5678",
    "title": "",
    "zip_code": "111-1111"
  },
  "destination_id": "XXXX-YYYY",
  "due_date": "2018-05-31T00:00:00+09:00",
  "email_flag": true,
  "issue_date": "2018-05-16T00:00:00+09:00",
  "posting_flag": false,
  "transaction_details": [
    {
      "amount": 2000,
      "description": "商品名A",
      "quantity": 2,
      "unit_price": 1000
    },
    {
      "amount": 160,
      "description": "消費税",
      "quantity": 1,
      "unit_price": 160
    }
  ],
  "user_defined_id": "transaction00000001"
}

Mfkessai::Transaction.create(request_body: params)
```

- Mfkessai::Transaction.retrieve

```
require "mfkessai"
Mfkessai.api_url = "https://sandbox"
Mfkessai.api_key = "mfkessai_test_..."

# 取引IDを指定して取得
Mfkessai::Transaction.retrieve("transation_test_123")
```

- Mfkessai::Transaction.cancel

```
require "mfkessai"
Mfkessai.api_url = "https://sandbox"
Mfkessai.api_key = "mfkessai_test_..."

# 取引をキャンセル

Mfkessai::Transaction.cancel("transation_test_123")
```

## Handling errors

```
begin
  # Use Mfkessai's library to make requests...
rescue Mfkessai::BadRequestError => e
  puts "Status is: #{e.http_status}"
  puts "Message is: #{e.message}"
  puts "http_body is: #{e.http_body}"
  puts "json_body is: #{e.json_body}"
rescue Mfkessai::AuthenticationError => e
  puts "Status is: #{e.http_status}"
  puts "Message is: #{e.message}"
  puts "http_body is: #{e.http_body}"
  puts "json_body is: #{e.json_body}"
rescue Mfkessai::ForbiddenError => e
  puts "Status is: #{e.http_status}"
  puts "Message is: #{e.message}"
  puts "http_body is: #{e.http_body}"
  puts "json_body is: #{e.json_body}"
rescue Mfkessai::NotFoundError => e
  puts "Status is: #{e.http_status}"
  puts "Message is: #{e.message}"
  puts "http_body is: #{e.http_body}"
  puts "json_body is: #{e.json_body}"
rescue Mfkessai::APIConnectionError => e
  puts "Status is: #{e.http_status}"
  puts "Message is: #{e.message}"
  puts "http_body is: #{e.http_body}"
  puts "json_body is: #{e.json_body}"
rescue Mfkessai::ServerError => e
  puts "Status is: #{e.http_status}"
  puts "Message is: #{e.message}"
  puts "http_body is: #{e.http_body}"
  puts "json_body is: #{e.json_body}"
rescue => e
   # Something else happened, completely unrelated to Mfkessai
end

```

## Development

Run all tests:

```
bundle exec rspec spec
```

## Contributing

1. Fork it
1. Create your feature branch (git checkout -b my-new-feature)
1. Commit your changes (git commit -am 'Add some feature')
1. Push to the branch (git push origin my-new-feature)
1. Create new Pull Request

## License

MIT
