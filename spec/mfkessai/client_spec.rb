# frozen_string_literal: true

require 'spec_helper'

module Mfkessai
  module ClientTest
    def self.request(url:, request_type:, request_body: nil)
      Mfkessai.orig_request(url: url, request_type: request_type, request_body: request_body)
    end
  end
end

RSpec.describe Mfkessai::Client do
  context 'error handle test' do
    it 'api key error' do
      allow(Mfkessai).to receive(:api_key).and_return(nil)
      expect{ Mfkessai::ClientTest.request(url: '/v1/customers', request_type: :get) }.to raise_error(Mfkessai::AuthenticationError)
    end

    it 'invalid response object from API' do
      expect{ Mfkessai::ClientTest.request(url: '/v1/customers', request_type: :get) }.to raise_error(Mfkessai::APIConnectionError)
    end

    it 'server error' do
      allow(Faraday).to receive(:new).and_return(Faraday::ConnectionFailed)
      expect{ Mfkessai::ClientTest.request(url: '/v1/customers', request_type: :get) }.to raise_error(Mfkessai::ServerError)
    end
  end
end
