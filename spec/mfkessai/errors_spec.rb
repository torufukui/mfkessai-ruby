# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Mfkessai::MfkessaiError do
  it "convert to string" do
    error = Mfkessai::MfkessaiError.new(message: 'error', http_status: 400)
    expect(error.to_s).to eq('(Status 400) error')
  end
end
