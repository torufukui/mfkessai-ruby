# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Mfkessai::Destination do
  it 'destination should be creatable' do
    params = {
      "address1": "千代田区1-2-3",
      "address2": "サンプルビル3F",
      "cc_emails": [
        "another.tanto1@example.jp",
        "another.tanto2@example.jp"
      ],
      "customer": {
        "office_name": "サンプル商事株式会社",
        "user_defined_id": "customer123456"
      },
      "department": "経理部",
      "email": "kesai.tanto@example.jp",
      "name": "請求先氏名",
      "prefecture": "東京都",
      "tel": "03-1234-5678",
      "title": "",
      "zip_code": "111-1111",
    }

    response = Mfkessai::Destination.create(request_body: params)
    expect(response.http_status).to eq(201)
  end
end
