# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Mfkessai::Transaction do
  it 'transaction should be listable' do
    response = Mfkessai::Transaction.list
    expect(response.http_status).to eq(200)
  end

  it 'transaction should be retrievable' do
    response = Mfkessai::Transaction.retrieve('P6GV-V4YN')
    expect(response.http_status).to eq(200)
  end

  it 'transaction should be creatable' do
    params = {
      "amount": 2160,
      "date": "2018-06-01T00:00:00+09:00",
      "destination": {
        "address1": "千代田区1-2-3",
        "address2": "サンプルビル3F",
        "cc_emails": [
          "another.tanto1@example.jp",
          "another.tanto2@example.jp"
        ],
        "customer": {
          "office_name": "サンプル商事株式会社",
          "user_defined_id": "customer1234567"
        },
        "department": "経理部",
        "email": "kesai.tanto@example.jp",
        "name": "請求先氏名",
        "prefecture": "東京都",
        "tel": "03-1234-5678",
        "title": "",
        "zip_code": "111-1111"
      },
      "due_date": "2018-07-31T00:00:00+09:00",
      "email_flag": true,
      "issue_date": "2018-06-16T00:00:00+09:00",
      "posting_flag": false,
      "transaction_details": [
        {
          "amount": 2000,
          "description": "商品名A",
          "quantity": 2,
          "unit_price": 1000
        },
        {
          "amount": 160,
          "description": "消費税",
          "quantity": 1,
          "unit_price": 160
        }
      ],
      "user_defined_id": "transaction0000000003"
    }
    response = Mfkessai::Transaction.create(request_body: params)
    expect(response.http_status).to eq(201)
  end

  it 'transaction should be cancelled' do
    response = Mfkessai::Transaction.cancel('46W3-EMA6')
    expect(JSON.parse(response.http_body)['status']).to eq('canceled')
    expect(response.http_status).to eq(200)
  end
end
