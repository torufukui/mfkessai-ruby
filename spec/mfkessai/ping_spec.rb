# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Mfkessai::Ping do
  it 'ping ok' do
    response = Mfkessai::Ping.send
    expect(response.http_body).to eq('ok')
  end
end
