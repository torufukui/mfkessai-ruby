# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Mfkessai::Examination do
  it 'examinations should be listable' do
    response = Mfkessai::Examination.list
    expect(response.http_status).to eq(200)
  end

  it 'examinations should be retrievable' do
    response = Mfkessai::Examination.retrieve('9NEY-R3VN')
    expect(response.http_status).to eq(200)
  end

  it 'examinations should be creatable' do
    params = {
      "amount": 2160,
      "destination_id": "A36P-3E7N",
      "due_date": "2018-07-31T00:00:00+09:00",
      "examination_details": [
        {
          "amount": 2000,
          "description": "商品名A",
          "quantity": 2,
          "unit_price": 1000
        },
        {
          "amount": 160,
          "description": "消費税",
          "quantity": 1,
          "unit_price": 160
        }
      ]
    }

    response = Mfkessai::Examination.create(request_body: params)
    expect(response.http_status).to eq(201)
  end

  it 'examination transaction should be creatable' do
    params = {
      "date": "2018-04-16T00:00:00+09:00",
      "email_flag": true,
      "issue_date": "2018-06-16T00:00:00+09:00",
      "posting_flag": false,
      "user_defined_id": "transaction00000005"
    }

    response = Mfkessai::Examination.create_transaction(id: '9NEY-R3VN', request_body: params)
    expect(response.http_status).to eq(201)
  end
end
