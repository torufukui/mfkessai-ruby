# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Mfkessai::Customer do
  it 'customers should be listable' do
    response = Mfkessai::Customer.list
    expect(response[:data][:customers].empty?).to be_falsey
  end

  it 'customer should be retrievable' do
    response = Mfkessai::Customer.retrieve("P6GV-RMAN")
    expect(response.http_status).to eq(200)
  end
end
