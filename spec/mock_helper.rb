# frozen_string_literal: true

module Mfkessai
  extend Mfkessai::Client

  class << self
    alias_method :orig_request, :request
  end

  # monkeypatch request methods
  module Client
    def request(url:, request_type:, request_body: nil)
      case url
      when '/v1/customers'
        MockData.test_list_customer
      when /\/v1\/customers\/\w+/
        MockData.test_retrieve_customer
      when '/v1/destinations'
        MockData.test_create_destination
      when '/v1/examinations'
        if request_type == :get
          MockData.test_list_examinations
        else
          MockData.test_create_examination
        end
      when /\/v1\/examinations\/\w.+\/transaction/
        MockData.test_create_transaction_examination
      when /\/v1\/examinations\/\w+/
        MockData.test_retrieve_examination
      when '/v1/ping'
        MockData.test_ping
      when '/v1/transactions'
        if request_type == :get
          MockData.test_list_transactions
        else
          MockData.test_create_transaction
        end
      when /\/v1\/transactions\/\w+/
        MockData.test_retrieve_transaction
      when /\/v1\/transactions\/\w+\/cancel/
        MockData.test_cancel_transaction
      end
    end
  end

  module MockData
    @mock_response = Struct.new(:data, :http_body, :http_headers, :http_status)

    def self.test_list_customer
      response = @mock_response.new
      response.data = {:customers=>[{:id=>"96EY-RWE6", :office_name=>"test", :user_defined_id=>"customer-20180526-8b35c507e4d8f07cbbb3b61a8223f54d"}]}
      response.http_status = 200
      response
    end

    def self.test_retrieve_customer
      response = @mock_response.new
      response.data = {:id=>"P6GV-RMAN", :office_name=>"test", :user_defined_id=>"customer-20180521-e88604b4edf97f3f728e1da84a8c8922"}
      response.http_status = 200
      response
    end

    def self.test_create_destination
      response = @mock_response.new
      response.data = {:address1=>"千代田区1-2-3", :address2=>"サンプルビル3F", :customer_id=>"Y9NV-MPW6", :department=>"経理部", :email=>"kesai.tanto@example.jp", :id=>"A36P-3E7N", :name=>"請求先氏名", :prefecture=>"東京都", :tel=>"03-1234-5678", :title=>"", :zip_code=>"111-1111"}
      response.http_body = "{\"address1\":\"千代田区1-2-3\",\"address2\":\"サンプルビル3F\",\"customer_id\":\"Y9NV-MPW6\",\"department\":\"経理部\",\"email\":\"kesai.tanto@example.jp\",\"id\":\"A36P-3E7N\",\"name\":\"請求先氏名\",\"prefecture\":\"東京都\",\"tel\":\"03-1234-5678\",\"title\":\"\",\"zip_code\":\"111-1111\"}\n"
      response.http_status = 201
      response
    end

    def self.test_list_examinations
      response = @mock_response.new
      response.data = {:examinations=>[{:destination_id=>"A36P-3E7N", :expires_at=>"2018-06-10T16:56:04+09:00", :fixed_at=>"2018-06-03T16:56:04+09:00", :id=>"9NEY-R3VN", :status=>"passed"}], :pagination=>{:page=>1, :per_page=>20, :total_count=>1, :total_pages=>1}}
      response.http_body = "{\"examinations\":[{\"destination_id\":\"A36P-3E7N\",\"expires_at\":\"2018-06-10T16:56:04+09:00\",\"fixed_at\":\"2018-06-03T16:56:04+09:00\",\"id\":\"9NEY-R3VN\",\"status\":\"passed\"}],\"pagination\":{\"page\":1,\"per_page\":20,\"total_count\":1,\"total_pages\":1}}\n"
      response.http_status = 200
      response
    end

    def self.test_create_examination
      response = @mock_response.new
      response.data = {:destination_id=>"A36P-3E7N", :expires_at=>"2018-06-10T16:56:04+09:00", :fixed_at=>"2018-06-03T16:56:04+09:00", :id=>"9NEY-R3VN", :status=>"passed"}
      response.http_body = "{\"destination_id\":\"A36P-3E7N\",\"expires_at\":\"2018-06-10T16:56:04+09:00\",\"fixed_at\":\"2018-06-03T16:56:04+09:00\",\"id\":\"9NEY-R3VN\",\"status\":\"passed\"}\n"
      response.http_status = 201
      response
    end

    def self.test_retrieve_examination
      response = @mock_response.new
      response.data = {:destination_id=>"A36P-3E7N", :expires_at=>"2018-06-10T16:56:04+09:00", :fixed_at=>"2018-06-03T16:56:04+09:00", :id=>"9NEY-R3VN", :status=>"passed"}
      response.http_body = "{\"destination_id\":\"A36P-3E7N\",\"expires_at\":\"2018-06-10T16:56:04+09:00\",\"fixed_at\":\"2018-06-03T16:56:04+09:00\",\"id\":\"9NEY-R3VN\",\"status\":\"passed\"}\n"
      response.http_status = 200
      response
    end

    def self.test_create_transaction_examination
      response = @mock_response.new
      response.data = {:amount=>2160, :destination_id=>"A36P-3E7N", :id=>"96R9-VPEN", :status=>"examination_passed", :user_defined_id=>"transaction00000005"}
      response.http_body="{\"amount\":2160,\"destination_id\":\"A36P-3E7N\",\"id\":\"96R9-VPEN\",\"status\":\"examination_passed\",\"user_defined_id\":\"transaction00000005\"}\n"
      response.http_status = 201
      response
    end

    def self.test_ping
      response = @mock_response.new
      response.http_body = 'ok'
      response.http_status = 200
      response
    end

    def self.test_list_transactions
      response = @mock_response.new
      response.data = {:pagination=>{:page=>1, :per_page=>20, :total_count=>326, :total_pages=>17}, :transactions=>[{:amount=>2160, :destination_id=>"A36P-3E7N", :id=>"4N33-Y4EN", :status=>"examination_passed", :user_defined_id=>"transaction0000000001"}, {:amount=>119340, :destination_id=>"7N4P-9VRN", :id=>"96R9-93PN", :status=>"examination_passed", :user_defined_id=>"transaction-20180526-8b35c507e4d8f07cbbb3b61a8223f54d"}]}
      response.http_body = "{\"pagination\":{\"page\":1,\"per_page\":20,\"total_count\":326,\"total_pages\":17},\"transactions\":[{\"amount\":2160,\"destination_id\":\"A36P-3E7N\",\"id\":\"4N33-Y4EN\",\"status\":\"examination_passed\",\"user_defined_id\":\"transaction0000000001\"}]}\n"
      response.http_status = 200
      response
    end

    def self.test_create_transaction
      response = @mock_response.new
      response.data = {:amount=>2160, :destination=>{:address1=>"千代田区1-2-3", :address2=>"サンプルビル3F", :customer_id=>"463A-9EG6", :department=>"経理部", :email=>"kesai.tanto@example.jp", :id=>"R6M7-AM46", :name=>"請求先氏名", :prefecture=>"東京都", :tel=>"03-1234-5678", :title=>"", :zip_code=>"111-1111"}, :destination_id=>"R6M7-AM46", :id=>"46W3-EMA6", :status=>"examination_passed", :transaction_details=>[{:amount=>2000, :description=>"商品名A", :quantity=>2, :unit_price=>1000}, {:amount=>160, :description=>"消費税", :quantity=>1, :unit_price=>160}], :user_defined_id=>"transaction0000000003"}
      response.http_body = "{\"amount\":2160,\"destination\":{\"address1\":\"千代田区1-2-3\",\"address2\":\"サンプルビル3F\",\"customer_id\":\"463A-9EG6\",\"department\":\"経理部\",\"email\":\"kesai.tanto@example.jp\",\"id\":\"R6M7-AM46\",\"name\":\"請求先氏名\",\"prefecture\":\"東京都\",\"tel\":\"03-1234-5678\",\"title\":\"\",\"zip_code\":\"111-1111\"},\"destination_id\":\"R6M7-AM46\",\"id\":\"46W3-EMA6\",\"status\":\"examination_passed\",\"transaction_details\":[{\"amount\":2000,\"description\":\"商品名A\",\"quantity\":2,\"unit_price\":1000},{\"amount\":160,\"description\":\"消費税\",\"quantity\":1,\"unit_price\":160}],\"user_defined_id\":\"transaction0000000003\"}\n"
      response.http_status = 201
      response
    end

    def self.test_retrieve_transaction
      response = @mock_response.new
      response.data = {:amount=>119340, :destination_id=>"7N7Y-EVMN", :id=>"P6GV-V4YN", :status=>"examination_passed", :user_defined_id=>"transaction-20180526-258295a1380a756e98ce8e53a94047be"}
      response.http_body = "{\"amount\":119340,\"destination_id\":\"7N7Y-EVMN\",\"id\":\"P6GV-V4YN\",\"status\":\"canceled\",\"user_defined_id\":\"transaction-20180526-258295a1380a756e98ce8e53a94047be\"}\n"
      response.http_status = 200
      response
    end

    def self.test_cancel_transaction
      response = @mock_response.new
      response.data = {:amount=>2160, :destination_id=>"R6M7-AM46", :id=>"46W3-EMA6", :status=>"canceled", :user_defined_id=>"transaction0000000003"}
      response.http_body = "{\"amount\":2160,\"destination_id\":\"R6M7-AM46\",\"id\":\"46W3-EMA6\",\"status\":\"canceled\",\"user_defined_id\":\"transaction0000000003\"}\n"
      response.http_status = 200
      response
    end
  end
end
