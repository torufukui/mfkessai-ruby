# frozen_string_literal: true
module Mfkessai
  class Transaction
    extend Mfkessai::Client

    # ref: https://mfkessai.co.jp/api_doc/endpoint.html#/transaction/transaction_index
    def self.list(page: nil, per_page: nil)
      url = '/v1/transactions'
      url += list_filter_parameters(page, per_page) if !page.nil? || !per_page.nil?
      request(url: url, request_type: :get)
    end

    # ref: https://mfkessai.co.jp/api_doc/endpoint.html#/transaction/transaction_create
    def self.create(request_body:)
      request(url: '/v1/transactions',
              request_type: :post,
              request_body: request_body)
    end

    # ref: https://mfkessai.co.jp/api_doc/endpoint.html#/transaction/transaction_show
    def self.retrieve(id)
      request(url: "/v1/transactions/#{id}", request_type: :get)
    end

    # ref: https://mfkessai.co.jp/api_doc/endpoint.html#/transaction/transaction_cancel
    def self.cancel(id)
      request(url: "/v1/transactions/#{id}/cancel", request_type: :post)
    end
  end
end
