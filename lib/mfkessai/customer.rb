# frozen_string_literal: true
module Mfkessai
  class Customer
    extend Mfkessai::Client

    # ref: https://mfkessai.co.jp/api_doc/endpoint.html#/customer/customer_index
    def self.list(page: nil, per_page: nil)
      url = '/v1/customers'
      url += list_filter_parameters(page, per_page) if !page.nil? || !per_page.nil?
      request(url: url, request_type: :get)
    end

    # ref: https://mfkessai.co.jp/api_doc/endpoint.html#/customer/customer_show
    def self.retrieve(id)
      request(url: "/v1/customers/#{id}", request_type: :get)
    end
  end
end
