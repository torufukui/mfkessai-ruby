# frozen_string_literal: true
module Mfkessai
  class Destination
    extend Mfkessai::Client

    # ref: https://mfkessai.co.jp/api_doc/endpoint.html#/destination/destination_create
    def self.create(request_body:)
      request(url: '/v1/destinations',
              request_type: :post,
              request_body: request_body)
    end
  end
end
