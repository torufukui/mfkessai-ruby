# frozen_string_literal: true
module Mfkessai
  class MfkessaiError < StandardError
    attr_reader :message, :http_status, :http_body, :json_body

    def initialize(message: nil, http_status: nil, http_body: nil, json_body: nil)
      @message = message
      @http_status = http_status
      @http_body = http_body
      @json_body = json_body
    end

    def to_s
      status_string = @http_status.nil? ? "" : "(Status #{@http_status}) "
      "#{status_string}#{@message}"
    end
  end

  # 400 Bad Request
  class BadRequestError < MfkessaiError
  end

  # 401 Unauthorized
  class AuthenticationError < MfkessaiError
  end

  # 403 Forbidden
  class ForbiddenError < MfkessaiError
  end

  # 404 NotFound
  class NotFoundError < MfkessaiError
  end

  # 500, 503 Internal Server Error
  class ServerError < MfkessaiError
  end

  # APIConnectionError is raised in the event that the SDK can't connect to
  # Mfkessai's servers.
  class APIConnectionError < MfkessaiError
  end
end
