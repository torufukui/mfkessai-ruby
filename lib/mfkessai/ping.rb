# frozen_string_literal: true
module Mfkessai
  class Ping
    extend Mfkessai::Client

    # ref: https://mfkessai.co.jp/api_doc/endpoint.html#/ping/ping_show
    def self.send
      request(url: '/v1/ping', request_type: :get)
    end
  end
end
