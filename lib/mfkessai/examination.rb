# frozen_string_literal: true
module Mfkessai
  class Examination
    extend Mfkessai::Client

    # ref: https://mfkessai.co.jp/api_doc/endpoint.html#/examination/examination_index
    def self.list(page: nil, per_page: nil)
      url = '/v1/examinations'
      url += list_filter_parameters(page, per_page) if !page.nil? || !per_page.nil?
      request(url: url, request_type: :get)
    end

    # ref: https://mfkessai.co.jp/api_doc/endpoint.html#/examination/examination_create
    def self.create(request_body:)
      request(url: '/v1/examinations',
              request_type: :post,
              request_body: request_body)
    end

    # ref: https://mfkessai.co.jp/api_doc/endpoint.html#/examination/examination_show
    def self.retrieve(id)
      request(url: "/v1/examinations/#{id}", request_type: :get)
    end

    # ref: https://mfkessai.co.jp/api_doc/endpoint.html#/examination/examination_create_transaction
    def self.create_transaction(id:, request_body:)
      request(url: "/v1/examinations/#{id}/transaction",
              request_type: :post,
              request_body: request_body)
    end
  end
end
