# frozen_string_literal: true
require 'net/http'
require 'openssl'
require 'json'
require 'faraday'

require 'mfkessai/version'
require 'mfkessai/client'
require 'mfkessai/customer'
require 'mfkessai/destination'
require 'mfkessai/examination'
require 'mfkessai/ping'
require 'mfkessai/transaction'
require 'mfkessai/errors'

module Mfkessai
  @api_version = 'v1'
  @open_timeout = 30
  @read_timeout = 80

  class << self
    attr_accessor :api_key, :api_url, :api_version,
                  :open_timeout, :read_timeout
  end

end
